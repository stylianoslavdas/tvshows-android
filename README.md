# TV-Shows-Android
# Introduction
App is a test for android.

### Technologies
The app is being developed using the latest Android technologies. It is written in Kotlin 1.6.10 and minSdkVersion 21 only devices with > Android 5.0 or greater.

The app mainly uses the MVVM pattern and uses the Dependency Injection pattern for easier dependency management and Unit Testing. The whole UI is written with jetpack compose.

The deployments and code signing are done exclusively using Android Studio and all the dependencies are being handled with Gradle.

Some of the technologies that were used are:
* Jetpack compose
* Dark and Light Theme
* ConstraintLayout (ShowItemContent.kt)
* MotionLayout (ShowDetailScreen.kt)
* Animations
* Navigation Compose
* Flow and coroutines
* Local data source with Room
* Dependency injection using Hilt
* UnitTest

# Getting Started
After you gain access to the Git, you need to follow the next steps and run the project on your machine. Please read all the steps before you attempt to execute them because you'll need to have a few things in handy.

1. Add your SSH key to your Git profile. You can read more on how to add your SSH key to your Git profile or how to generate a new one if you don't have one on this link.
2. Clone the repo to a location of your choosing.
3. Open root folder with Android studio, select Build Variants and Run app.

# Architecture
#### Android Architecture Blueprints v2
The app is being developed mainly with the Android Architecture Blueprints v2 <strong>MVVM</strong> pattern. Example by android [github](https://github.com/android/architecture-samples).

### UI
The UI of the app is being developed exclusively jetpack compose. The reasons behind this decision are:
* Easier Git conflicts resolve
* Explicit usage of the Dependency Injection pattern
* Explicit UI logic for each UI component with Navigation component.
* Better and easier to handle architecture for building complicated views

### Dependency injection
The app handles the passing of data to each `class` with the <strong>Dependency Injection</strong>. Each instance requires through its initializer every dependency it needs to function. This pattern helps the developer avoid bugs due to forgetting to provide dependencies after the initialization of the instance. Also, this pattern allows the developer to easily mock dependencies and write Unit Tests easier.

### DTOs (Data Transfer Object)
Every app needs to communicate with some sort of an API to perform its functionalities. The developer needs to decode or encode domain models to the format the API understands. Usually in JSON. More often than not, many developers tend to use the same `class` that they use to communicate with the API as domain models. The problem with this approach is that if the API specifications change (extra fields, field renaming, structure change), the developer needs to make changes to every place that this particular model was used. In some cases this is trivial but most of the time it requires a significant amount of work. Another problem with this approach is that the developer is enforced to use an object that is not very easy to work with just because it needs to match the API specification. This leads to worse architecture.

### Repositories and Logic Controllers
The <strong>Repository</strong> pattern is another pattern that is used to abstract business logic with the serialization of the data or the communication with an API. This pattern consists of a `interface` that defines the methods that should be implemented and then the Logic Controller which is usually a `class` conforms to that interface. The whole point of this pattern is to allow view controllers to accept a `interface` as a dependency (the repository) without relying upon a concrete implementation. Whoever initializes that view controller will be responsible to provide a concrete implementation of that dependency but that view controller does not know that. This avoids coupling and allows the developer to write Unit Tests easier.

# Environments
The app currently supports the following environments (productFlavors).

* Production
* Stage

### Differences between environments
These environments make the development and the testing of multiple versions of the app faster. It makes more sense for this project because the only things that are different for each environment are the following.

* App name
* Application ID
* Base URL
* VersionCode
* VersionName

### Environment architecture
The different environments were added mainly by using this [guide](https://medium.com/easyfundraising-org-uk/setting-up-for-different-environments-in-android-studio-using-build-variants-product-756fdf8d47fb). There are differences described below.
