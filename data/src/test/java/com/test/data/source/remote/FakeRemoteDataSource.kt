package com.test.data.source.remote

import com.test.data.Result
import com.test.data.Result.Success
import com.test.data.Result.Error
import com.test.data.model.local.Show

class FakeRemoteDataSource(var shows: MutableList<Show>? = mutableListOf()) : MyRemoteDataSource {

    override suspend fun getShows(): Result<List<Show>> {
        shows?.let { return Success(ArrayList(it)) }
        return Error(Exception("Tasks not found"))
    }

}
