/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.test.data.source.local

import com.test.data.Result
import com.test.data.Result.Success
import com.test.data.Result.Error
import com.test.data.model.local.Show
import kotlinx.coroutines.flow.Flow

class FakeLocalDataSource(var shows: MutableList<Show>? = mutableListOf()) : MyLocalDataSource {

    override fun getShowsStream(): Flow<List<Show>> {
        TODO("Not yet implemented")
    }

    override suspend fun getShow(showId: Long): Result<Show> {
        shows?.firstOrNull { it.id == showId }?.let { return Success(it) }
        return Error(
            Exception("Task not found")
        )
    }

    override suspend fun haveShows(): Boolean {
        return !shows.isNullOrEmpty()
    }

    override suspend fun saveShows(shows: List<Show>) {
        this.shows?.addAll(shows)
    }

    override suspend fun deleteAllShows() {
        shows?.clear()
    }

}
