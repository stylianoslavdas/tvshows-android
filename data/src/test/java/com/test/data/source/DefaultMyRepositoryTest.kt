package com.test.data.source

import com.test.data.MainCoroutineRule
import com.test.data.Result.Success
import com.test.data.model.local.Show
import com.test.data.source.local.FakeLocalDataSource
import com.test.data.source.remote.FakeRemoteDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test

/**
 * Unit tests for the implementation of the in-memory repository with cache.
 */
@ExperimentalCoroutinesApi
class DefaultMyRepositoryTest {

    private val show1 = Show.dummy(1)
    private val show2 = Show.dummy(2)
    private val show3 = Show.dummy(3)
    private val newShow = Show.dummy(4)
    private val remoteShows = listOf(show1, show2).sortedBy { it.rating }
    private val localShows = listOf(show3).sortedBy { it.rating }
    private val newShows = listOf(show3).sortedBy { it.rating }
    private lateinit var myRemoteDataSource: FakeRemoteDataSource
    private lateinit var myLocalDataSource: FakeLocalDataSource

    // Class under test
    private lateinit var myRepository: DefaultMyRepository

    // Set the main coroutines dispatcher for unit testing.
    @ExperimentalCoroutinesApi
    @get:Rule
    val mainCoroutineRule = MainCoroutineRule()

    @ExperimentalCoroutinesApi
    @Before
    fun createRepository() {
        myRemoteDataSource = FakeRemoteDataSource(remoteShows.toMutableList())
        myLocalDataSource = FakeLocalDataSource(localShows.toMutableList())
        // Get a reference to the class under test
        myRepository = DefaultMyRepository(
            myRemoteDataSource, myLocalDataSource, Dispatchers.Main
        )
    }

    @ExperimentalCoroutinesApi
    @Test
    fun getShows_emptyRepositoryAndUninitializedCache() = runTest {
        val myRepository = DefaultMyRepository(
            FakeRemoteDataSource(), FakeLocalDataSource(), Dispatchers.Main
        )

        assertThat(myRepository.fetchShows() is Success).isTrue()
    }

    @Test
    fun getShows_repositoryCachesAfterFirstApiCall() = runTest {
        // Trigger the repository to load data, which loads from remote and caches
       myRepository.fetchShows()

        val initial = myLocalDataSource.shows

        myRemoteDataSource.shows = newShows.toMutableList()

        myRepository.fetchShows()

        val second = myLocalDataSource.shows

        // Initial and second should match
        assertThat(second).isEqualTo(initial)
    }

    @Test
    fun getShows_requestsShowsFromRemoteDataSource() = runTest {
        // When shows are requested from the tasks repository
        myRepository.fetchShows() as Success

        val shows = myLocalDataSource.shows

        // Then shows are loaded from the remote data source
        assertThat(shows).isEqualTo(remoteShows)
    }
}