package com.test.data.network.mock

import android.content.Context
import com.test.data.model.remote.ShowDto
import com.test.data.network.NetworkAPI
import retrofit2.Call

class MockNetworkAPI(private val context: Context) : NetworkAPI {

    override fun getShows(): Call<List<ShowDto>> {
        TODO("Not yet implemented")
    }

    companion object {
        private const val MOCK_SERVICE_DELAY_MILLIS: Long = 500
    }

}