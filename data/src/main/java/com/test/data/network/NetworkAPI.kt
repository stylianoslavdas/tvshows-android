package com.test.data.network

import com.test.data.model.remote.ShowDto
import retrofit2.Call
import retrofit2.http.GET

interface NetworkAPI {

    @GET("/shows")
    fun getShows(): Call<List<ShowDto>>
}