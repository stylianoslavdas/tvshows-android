package com.test.data.network.staging

import com.test.data.model.remote.ShowDto
import com.test.data.network.NetworkAPI
import retrofit2.Call
import retrofit2.http.GET

class StagingNetworkAPI : NetworkAPI {

    @GET(ENDPOINT_SHOWS)
    override fun getShows(): Call<List<ShowDto>> {
        TODO("Not yet implemented")
    }

    companion object {
        private const val ENDPOINT_SHOWS = "shows"
    }

}