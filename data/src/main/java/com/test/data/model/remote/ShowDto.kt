package com.test.data.model.remote

import com.google.gson.annotations.SerializedName

data class ShowDto(
    @SerializedName("id") val id: Long? = null,
    @SerializedName("url") val url: String? = null,
    @SerializedName("name") val name:  String? = null,
    @SerializedName("type") val type: String? = null,
    @SerializedName("language") val language: String? = null,
    @SerializedName("genres") val genres: List<String>? = null,
    @SerializedName("status") val status: String? = null,
    @SerializedName("rating") val rating: RatingDto? = null,
    @SerializedName("runtime") val runtime: Int? = null,
    @SerializedName("premiered") val premiered: String? = null,
    @SerializedName("officialSite") val officialSite: String? = null,
    @SerializedName("image") val image: ImageDto? = null,
    @SerializedName("summary") val summary: String? = null
) {
    data class ImageDto(
        @SerializedName("medium") val medium: String? = null,
        @SerializedName("original") val original: String? = null
    )

    data class RatingDto(
        @SerializedName("average") val average: Double? = null
    )
}