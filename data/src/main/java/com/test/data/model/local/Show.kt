package com.test.data.model.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

/**
 * Immutable model class for a Show. In order to compile with Room
 *
 * @param id id of the show
 * @param url url of the show
 * @param name name of the show
 * @param type type of the show
 * @param language language of the show
 * @param genres genres of the show
 * @param status status of the show
 * @param runtime runtime of the show
 * @param rating rating of the show
 * @param premiered premiered of the show
 * @param officialSite officialSite of the show
 * @param image image of the show
 * @param summary summary of the show
 */
@Entity(tableName = "shows")
data class Show constructor(
    @PrimaryKey @ColumnInfo(name = "entryId") val id: Long,
    @ColumnInfo(name = "url") val url: String,
    @ColumnInfo(name = "name") val name:  String,
    @ColumnInfo(name = "type") val type: String,
    @ColumnInfo(name = "language") val language: String,
    @ColumnInfo(name = "genres") val genres: List<String>,
    @ColumnInfo(name = "status") val status: String,
    @ColumnInfo(name = "runtime") val runtime: Int,
    @ColumnInfo(name = "rating") val rating: Double,
    @ColumnInfo(name = "premiered") val premiered: Date,
    @ColumnInfo(name = "officialSite") val officialSite: String,
    @ColumnInfo(name = "image") val image: Image,
    @ColumnInfo(name = "summary") val summary: String
) {
    data class Image(
        val medium: String,
        val original: String
    )

    companion object {
        fun dummy(id: Long): Show {
            return Show(
                id = id,
                url = "https://www.tvmaze.com/shows/1/under-the-dome",
                name = "Under the Dome",
                type = "Scripted",
                language = "English",
                genres = listOf("Drama", "Science-Fiction", "Thriller"),
                status = "Ended",
                runtime = 60,
                rating = 6.5,
                premiered = Date(),
                officialSite = "http://www.cbs.com/shows/under-the-dome/",
                image = Image(
                    medium = "https://static.tvmaze.com/uploads/images/medium_portrait/81/202627.jpg",
                    original = "https://static.tvmaze.com/uploads/images/original_untouched/81/202627.jpg"
                ),
                summary = "<p><b>Under the Dome</b> is the story of a small town that is suddenly and inexplicably sealed off from the rest of the world by an enormous transparent dome. The town's inhabitants must deal with surviving the post-apocalyptic conditions while searching for answers about the dome, where it came from and if and when it will go away.</p>"
            )
        }
    }
}