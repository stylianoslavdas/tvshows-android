package com.test.data.mapper

import com.test.data.model.local.Show
import com.test.data.model.remote.ShowDto
import java.text.SimpleDateFormat
import java.util.*

fun ShowDto.toShow(): Show? {
    val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    val premieredDate: Date? = premiered?.let {
        dateFormat.parse(it)
    }
    val ratingShow = rating?.toRating()
    val imageShow = image?.toImage()
    return if (
        id == null ||
        url == null ||
        name == null ||
        genres == null ||
        ratingShow == null ||
        runtime == null ||
        premieredDate == null ||
        officialSite == null ||
        imageShow == null ||
        summary == null
    ) {
        null
    } else Show(
        id = id,
        url = url,
        name = name,
        type = type ?: "-",
        language = language ?: "-",
        genres = genres,
        status = status ?: "-",
        rating = ratingShow,
        runtime = runtime,
        premiered = premieredDate,
        officialSite = officialSite,
        image = imageShow,
        summary = summary
    )
}

private fun ShowDto.RatingDto.toRating(): Double? {
    return average
}

private fun ShowDto.ImageDto.toImage(): Show.Image? {
    return if (
        medium == null ||
        original == null
    ) {
        null
    } else Show.Image(
        medium = medium,
        original = original
    )
}