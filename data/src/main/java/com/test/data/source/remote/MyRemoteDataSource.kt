package com.test.data.source.remote

import com.test.data.model.local.Show
import com.test.data.Result

/**
 * Main entry point for accessing shows data.
 */
interface MyRemoteDataSource {

    suspend fun getShows(): Result<List<Show>>
}