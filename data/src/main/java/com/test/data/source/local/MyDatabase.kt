package com.test.data.source.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.test.data.model.local.Show

/**
 * The Room Database that contains the Show table.
 *
 * Note that exportSchema should be true in production databases.
 */
@Database(
    entities = [
        Show::class
    ],
    version = 1,
    exportSchema = false
)
@TypeConverters(RoomConverter::class)
abstract class MyDatabase : RoomDatabase() {

    abstract fun showsDao(): ShowsDao
}