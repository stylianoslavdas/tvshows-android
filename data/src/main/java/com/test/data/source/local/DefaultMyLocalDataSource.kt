package com.test.data.source.local

import com.test.data.Result
import com.test.data.model.local.Show
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import com.test.data.Result.Success
import com.test.data.Result.Error

/**
 * Concrete implementation of a data source as a db.
 */
class DefaultMyLocalDataSource(
    private val showsDao: ShowsDao,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
): MyLocalDataSource {

    override fun getShowsStream(): Flow<List<Show>> {
        return showsDao.observeShows()
    }

    override suspend fun getShow(showId: Long): Result<Show> = withContext(ioDispatcher) {
        try {
            val show = showsDao.getShowById(showId)
            if (show != null) {
                return@withContext Success(show)
            } else {
                return@withContext Error(Exception("Show not found!"))
            }
        } catch (e: Exception) {
            return@withContext Error(e)
        }
    }

    override suspend fun haveShows(): Boolean = withContext(ioDispatcher) {
        return@withContext showsDao.haveShows()
    }

    override suspend fun saveShows(shows: List<Show>) = withContext(ioDispatcher) {
        showsDao.insertShows(shows)
    }

    override suspend fun deleteAllShows() = withContext(ioDispatcher) {
        showsDao.deleteShows()
    }
}