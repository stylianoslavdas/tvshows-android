package com.test.data.source

import com.test.data.source.local.MyLocalDataSource
import com.test.data.source.remote.MyRemoteDataSource
import com.test.data.model.local.Show
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import com.test.data.Result
import com.test.data.Result.Success
import com.test.data.Result.Error

/**
 * Default implementation of [MyRepository]. Single entry point for managing shows' data.
 */
class DefaultMyRepository(
    private val myRemoteDataSource: MyRemoteDataSource,
    private val myLocalDataSource: MyLocalDataSource,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
): MyRepository {

    override fun getShowsStream(): Flow<List<Show>> {
        return myLocalDataSource.getShowsStream()
    }

    override suspend fun checkShowsFetch(): Result<Unit> {
        if (!myLocalDataSource.haveShows()) {
            return fetchShows()
        }
        return Success(Unit)
    }

    override suspend fun fetchShows(): Result<Unit> {
        return when(val remoteShows = myRemoteDataSource.getShows()) {
            is Success -> {
                myLocalDataSource.deleteAllShows()
                myLocalDataSource.saveShows(remoteShows.data)
                Success(Unit)
            }
            is Error -> {
                Error(remoteShows.exception)
            }
        }
    }

    override suspend fun getShowById(showId: Long): Result<Show> {
        return myLocalDataSource.getShow(showId)
    }
}