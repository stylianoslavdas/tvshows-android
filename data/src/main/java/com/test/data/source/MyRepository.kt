package com.test.data.source

import com.test.data.model.local.Show
import kotlinx.coroutines.flow.Flow
import com.test.data.Result

/**
 * Interface to the data layer.
 */
interface MyRepository {

    fun getShowsStream(): Flow<List<Show>>

    suspend fun checkShowsFetch(): Result<Unit>

    suspend fun fetchShows(): Result<Unit>

    suspend fun getShowById(showId: Long): Result<Show>
}