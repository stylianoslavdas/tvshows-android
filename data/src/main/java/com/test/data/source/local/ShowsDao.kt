package com.test.data.source.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.test.data.model.local.Show
import kotlinx.coroutines.flow.Flow

/**
 * Data Access Object for the shows table.
 */
@Dao
interface ShowsDao {

    /**
     * Observes list of shows.
     *
     * @return all shows.
     */
    @Query("SELECT * FROM Shows ORDER BY rating DESC")
    fun observeShows(): Flow<List<Show>>

    /**
     * Select all shows from the shows table.
     *
     * @return have shows.
     */
    @Query("SELECT EXISTS(SELECT * FROM Shows)")
    suspend fun haveShows(): Boolean

    /**
     * Select a show by id.
     *
     * @param showId the show id.
     * @return the show with showId.
     */
    @Query("SELECT * FROM Shows WHERE entryId = :showId")
    suspend fun getShowById(showId: Long): Show?

    /**
     * Insert a show in the database. If the show already exists, replace it.
     *
     * @param show the show to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertShow(show: Show)

    /**
     * Insert shows in the database. If the show already exists, replace it.
     *
     * @param shows the shows to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertShows(shows: List<Show>)

    /**
     * Delete all shows.
     */
    @Query("DELETE FROM Shows")
    suspend fun deleteShows()
}