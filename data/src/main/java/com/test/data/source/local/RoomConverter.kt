package com.test.data.source.local

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.test.data.model.local.Show
import java.text.SimpleDateFormat
import java.util.*

class RoomConverter {
    companion object {
        @Suppress("NOTHING_TO_INLINE")
        private inline fun <T : Enum<T>> T.toInt(): Int = this.ordinal
        private inline fun <reified T : Enum<T>> Int.toEnum(): T = enumValues<T>()[this]

        @TypeConverter
        @JvmStatic
        fun genresToString(value: List<String>): String = Gson().toJson(value)
        @TypeConverter
        @JvmStatic
        fun stringToGenres(value: String): List<String> = Gson().fromJson(value, object : TypeToken<List<String>>() {}.type)

        @TypeConverter
        @JvmStatic
        fun imageToString(value: Show.Image): String = Gson().toJson(value)
        @TypeConverter
        @JvmStatic
        fun stringToImage(value: String): Show.Image = Gson().fromJson(value, Show.Image::class.java)

        @TypeConverter
        @JvmStatic
        fun dateToString(value: Date?): String {
            return try { if (value != null) SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault()).format(value) else "" } catch (e: Exception) { "" }
        }
        @TypeConverter
        @JvmStatic
        fun stringToDate(value: String): Date? {
            return if (value.isNotEmpty()) SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault()).parse(value) else Date()
        }
    }
}