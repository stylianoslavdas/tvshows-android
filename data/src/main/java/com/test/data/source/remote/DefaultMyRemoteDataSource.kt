package com.test.data.source.remote

import com.test.data.model.local.Show
import com.test.data.network.NetworkAPI
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import com.test.data.Result
import com.test.data.Result.Success
import com.test.data.Result.Error
import com.test.data.mapper.toShow

/**
 * Implementation of the data source that adds a network.
 */
class DefaultMyRemoteDataSource(
    private val api: NetworkAPI,
    private val defaultDispatcher: CoroutineDispatcher = Dispatchers.Default
): MyRemoteDataSource {

    override suspend fun getShows(): Result<List<Show>> = withContext(defaultDispatcher) {
        return@withContext try {
            val showsResponse = api.getShows().execute()
            val shows = mutableListOf<Show>()
            if (showsResponse.isSuccessful && showsResponse.body() != null) {
                showsResponse.body()?.forEach { showsDto ->
                    showsDto.toShow()?.let { shows.add(it) }
                }
                Success(shows)
            } else {
                Error(Exception(showsResponse.message()))
            }
        } catch (exception: Exception) {
            Error(exception)
        }
    }
}