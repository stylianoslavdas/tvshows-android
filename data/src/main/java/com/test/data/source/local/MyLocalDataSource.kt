package com.test.data.source.local

import com.test.data.Result
import com.test.data.model.local.Show
import kotlinx.coroutines.flow.Flow

/**
 * Main entry point for accessing shows data.
 */
interface MyLocalDataSource {

    fun getShowsStream(): Flow<List<Show>>

    suspend fun getShow(showId: Long): Result<Show>

    suspend fun haveShows(): Boolean

    suspend fun saveShows(shows: List<Show>)

    suspend fun deleteAllShows()
}