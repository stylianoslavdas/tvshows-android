package com.test.tvshows.mapper

import com.test.data.model.local.Show
import com.test.tvshows.model.ViewShow

fun List<Show>.toViewModel(): List<ViewShow> {
    val viewShows = mutableListOf<ViewShow>()
    forEach {
        viewShows.add(it.toViewModel())
    }
    return viewShows
}

fun Show.toViewModel(): ViewShow {
    return ViewShow(
        id = id,
        url = url,
        name = name,
        type = type,
        language = language,
        genres = genres,
        status = status,
        runtime = runtime,
        rating = rating,
        premiered = premiered,
        officialSite = officialSite,
        image = image.toViewModel(),
        summary = summary
    )
}

private fun Show.Image.toViewModel(): ViewShow.Image {
    return ViewShow.Image(
        medium = medium,
        original = original
    )
}