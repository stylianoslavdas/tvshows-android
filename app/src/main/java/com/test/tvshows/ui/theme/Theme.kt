package com.test.tvshows.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable

private val DarkColorPalette = darkColors(
    primary = BasicGreen,
    primaryVariant = BasicRed,
    secondary = BasicGreen
)

private val LightColorPalette = lightColors(
    primary = BasicRed,
    primaryVariant = BasicGreen,
    secondary = BasicRedLight
)

@Composable
fun TVShowsTheme(darkTheme: Boolean = isSystemInDarkTheme(), content: @Composable () -> Unit) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}