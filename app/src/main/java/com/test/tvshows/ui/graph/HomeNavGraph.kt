package com.test.tvshows.ui.graph

import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable
import androidx.navigation.navigation
import com.test.tvshows.ui.showdetail.ShowDetailScreen
import com.test.tvshows.ui.shows.ShowsScreen

fun NavGraphBuilder.homeNavGraph(
    navController: NavHostController,
    navActions: HomeNavigationActions
) {
    navigation(
        route = Graph.HOME,
        startDestination = HomeDestinations.SHOWS_ROOT
    ) {
        composable(route = HomeDestinations.SHOWS_ROOT) {
            ShowsScreen(
                onShowClick = { show ->
                    navActions.navigateToShowDetail(show.id.toString())
                },
                onFinish = { activity ->
                    activity.finish()
                }
            )
        }
        composable(route = HomeDestinations.SHOW_DETAIL_ROUTE) {
            ShowDetailScreen(
                onBack = {
                    navController.popBackStack()
                }
            )
        }
    }
}