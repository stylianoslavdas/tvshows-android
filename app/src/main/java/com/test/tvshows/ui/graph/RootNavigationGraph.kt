package com.test.tvshows.ui.graph

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.rememberNavController

@Composable
fun RootNavigationGraph(
    navController: NavHostController = rememberNavController(),
    navActions: HomeNavigationActions = remember (navController) {
        HomeNavigationActions(navController)
    }
) {
    NavHost(
        navController = navController,
        route = Graph.ROOT,
        startDestination = Graph.HOME
    ) {
        homeNavGraph(
            navController = navController,
            navActions = navActions
        )
    }
}

object Graph {
    const val ROOT = "root_graph"
    const val HOME = "home_graph"
}