package com.test.tvshows.ui.shows

import androidx.annotation.StringRes
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.test.tvshows.R
import com.test.tvshows.ui.theme.TVShowsTheme

@Composable
fun ShowsInfoContent(
    nShows: Int,
    nTypeScripted: Int,
    nTypeAnimation: Int,
    nTypeReality: Int,
    nTypeTalkShow: Int,
    modifier: Modifier = Modifier,
    firstTranslationX: Dp = 500.dp,
) {
    var translationXState by remember { mutableStateOf(firstTranslationX) }
    val translationX by animateDpAsState(
        targetValue = translationXState,
        tween(durationMillis = 1500)
    )
    LaunchedEffect(key1 = nShows) {
        translationXState = 0.dp
    }

    Card(
        elevation = 8.dp,
        shape = RoundedCornerShape(16.dp),
        modifier = modifier
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier.padding(16.dp)
        ) {
            Text(
                text = stringResource(R.string.shows_info_title),
                style = MaterialTheme.typography.h6
            )
            Divider(modifier = Modifier.padding(8.dp))
            InfoItemContent(title = R.string.shows_info_nShows, value = nShows.toString(), translationX)
            InfoItemContent(title = R.string.shows_info_nTypeScripted, value = nTypeScripted.toString(), -translationX)
            InfoItemContent(title = R.string.shows_info_nTypeAnimation, value = nTypeAnimation.toString(), translationX)
            InfoItemContent(title = R.string.shows_info_nTypeReality, value = nTypeReality.toString(), -translationX)
            InfoItemContent(title = R.string.shows_info_nTypeTalkShow, value = nTypeTalkShow.toString(), translationX)
        }

    }
}

@Composable
private fun InfoItemContent(@StringRes title: Int, value: String, translationX: Dp) {
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier
            .graphicsLayer {
                this.translationX = translationX.toPx()
            }
            .fillMaxWidth()
            .padding(8.dp)
    ) {
        Text(
            text = stringResource(title),
            style = MaterialTheme.typography.subtitle1,
        )
        Text(
            text = value,
            style = MaterialTheme.typography.body2,
        )
    }
}

@Preview(showBackground = true)
@Composable
private fun ShowsInfoContentPreview() {
    TVShowsTheme(darkTheme = false) {
        Surface {
            ShowsInfoContent(
                nShows = 10,
                nTypeScripted = 5,
                nTypeAnimation = 5,
                nTypeReality = 0,
                nTypeTalkShow = 0,
                modifier = Modifier.fillMaxWidth(),
                firstTranslationX = 0.dp
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun ShowsInfoContentDarkPreview() {
    TVShowsTheme(darkTheme = true) {
        Surface {
            ShowsInfoContent(
                nShows = 10,
                nTypeScripted = 5,
                nTypeAnimation = 5,
                nTypeReality = 0,
                nTypeTalkShow = 0,
                modifier = Modifier.fillMaxWidth(),
                firstTranslationX = 0.dp
            )
        }
    }
}