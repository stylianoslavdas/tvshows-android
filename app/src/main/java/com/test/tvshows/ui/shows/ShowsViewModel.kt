package com.test.tvshows.ui.shows

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.test.data.Result.Success
import com.test.data.Result.Error
import com.test.data.source.MyRepository
import com.test.tvshows.mapper.toViewModel
import com.test.tvshows.model.ViewShow
import com.test.tvshows.util.WhileUiSubscribed
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * UiState for the show list screen.
 */
data class ShowsUiState(
    val shows: List<ViewShow> = emptyList(),
    val loading: Boolean = false,
    val message: String? = null
)

/**
 * ViewModel for the show list screen.
 */
@HiltViewModel
class ShowsViewModel @Inject constructor(
    private val myRepository: MyRepository
) : ViewModel() {

    private val _shows = myRepository.getShowsStream()
    private val _loading = MutableStateFlow(false)
    private val _message: MutableStateFlow<String?> = MutableStateFlow(null)

    val uiState: StateFlow<ShowsUiState> = combine(
        _shows, _loading, _message
    ) { shows, loading, message ->
        ShowsUiState(
            shows = shows.toViewModel(),
            loading = loading,
            message = message
        )
    }
        .stateIn(
            scope = viewModelScope,
            started = WhileUiSubscribed,
            initialValue = ShowsUiState(loading = true)
        )

    init {
        checkShowsFetch()
    }

    fun snackbarMessageShown() {
        _message.value = null
    }

    private fun showSnackbarMessage(message: String) {
        _message.value = message
    }

    private fun checkShowsFetch() {
        _loading.value = true
        viewModelScope.launch {
            when(val checkShowsFetchResults = myRepository.checkShowsFetch()) {
                is Success -> {
                    _loading.value = false
                }
                is Error -> {
                    showSnackbarMessage(checkShowsFetchResults.exception.message.toString())
                    _loading.value = false
                }
            }
        }
    }

    fun refresh() {
        _loading.value = true
        viewModelScope.launch {
            when(val fetchShowsResult = myRepository.fetchShows()) {
                is Success -> {
                    _loading.value = false
                }
                is Error -> {
                    showSnackbarMessage(fetchShowsResult.exception.message.toString())
                    _loading.value = false
                }
            }
        }
    }
}