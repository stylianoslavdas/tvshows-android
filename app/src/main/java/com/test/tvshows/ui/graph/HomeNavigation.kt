package com.test.tvshows.ui.graph

import com.test.tvshows.MainActivity
import androidx.navigation.NavHostController
import com.test.tvshows.ui.graph.HomeScreens.SHOW_DETAIL_SCREEN
import com.test.tvshows.ui.graph.HomeDestinationsArgs.SHOW_ID_ARG
import com.test.tvshows.ui.graph.HomeScreens.SHOWS_SCREEN

/**
 * Screens used in [HomeDestinations]
 */
private object HomeScreens {
    const val SHOWS_SCREEN = "shows"
    const val SHOW_DETAIL_SCREEN = "show"
}
/**
 * Arguments used in [HomeDestinations] routes
 */
object HomeDestinationsArgs {
    const val SHOW_ID_ARG = "showId"
}

/**
 * Destinations used in the [MainActivity]
 */
object HomeDestinations {
    const val SHOWS_ROOT = SHOWS_SCREEN
    const val SHOW_DETAIL_ROUTE = "${SHOW_DETAIL_SCREEN}/{$SHOW_ID_ARG}"
}

/**
 * Models the navigation actions in the app.
 */
class HomeNavigationActions(private val navController: NavHostController) {

    fun navigateToShowDetail(showId: String) {
        navController.navigate("$SHOW_DETAIL_SCREEN/$showId")
    }
}