package com.test.tvshows.ui.shows

import androidx.annotation.StringRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.test.tvshows.R
import com.test.tvshows.ui.theme.TVShowsTheme

@Composable
fun ShowsEmptyContent(
    @StringRes noShowsLabel: Int,
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            painter = painterResource(id = R.drawable.ic_data_object),
            contentDescription = stringResource(R.string.generic_noData),
            colorFilter = ColorFilter.tint(MaterialTheme.colors.onBackground),
            modifier = Modifier.size(96.dp)
        )
        Text(stringResource(id = noShowsLabel))
    }
}

@Preview(showBackground = true)
@Composable
private fun ShowsEmptyContentPreview() {
    TVShowsTheme(darkTheme = false) {
        Surface {
            ShowsEmptyContent(
                noShowsLabel = R.string.generic_noData
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun ShowsEmptyContentDarkPreview() {
    TVShowsTheme(darkTheme = true) {
        Surface {
            ShowsEmptyContent(
                noShowsLabel = R.string.generic_noData
            )
        }
    }
}