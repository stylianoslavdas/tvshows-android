package com.test.tvshows.ui.shows

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import coil.compose.AsyncImage
import com.test.tvshows.R
import com.test.tvshows.model.ViewShow
import com.test.tvshows.ui.theme.TVShowsTheme
import com.test.tvshows.util.HtmlText

@Composable
fun ShowItemContent(
    show: ViewShow,
    onClickShow: (ViewShow) -> Unit
) {
    ConstraintLayout(
        modifier = Modifier
            .fillMaxWidth()
            .clickable {
                onClickShow(show)
            }
            .background(MaterialTheme.colors.background)
            .padding(horizontal = dimensionResource(id = R.dimen.horizontal_margin))
    ) {
        val (imageRef,
            nameSummaryRef,
            ratingRef
        ) = createRefs()

        AsyncImage(
            model = show.image.medium,
            contentDescription = "contentDescription",
            modifier = Modifier
                .constrainAs(imageRef) {
                    start.linkTo(parent.start)
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                }.height(100.dp).width(70.dp)
        )
        Column(
            Modifier
                .constrainAs(nameSummaryRef) {
                    start.linkTo(imageRef.end, margin = 16.dp)
                    end.linkTo(ratingRef.start, margin = 8.dp)
                    top.linkTo(imageRef.top)
                    bottom.linkTo(imageRef.bottom)
                    width = Dimension.fillToConstraints
                }
        ) {
            Text(
                text = show.name,
                style = MaterialTheme.typography.subtitle1,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
            )
            HtmlText(
                html = show.summary,
                style = MaterialTheme.typography.caption,
                textColor = MaterialTheme.colors.onBackground,
                maxLines = 2
            )
        }
        Text(
            text = show.rating.toString(),
            style = MaterialTheme.typography.subtitle2,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            modifier = Modifier.constrainAs(ratingRef) {
                end.linkTo(parent.end)
                top.linkTo(parent.top)
                bottom.linkTo(parent.bottom)
            }
        )
    }
}

@Preview(showBackground = true)
@Composable
private fun ShowItemContentPreview() {
    TVShowsTheme(darkTheme = false) {
        Surface {
            ShowItemContent(
                show = ViewShow.dummy(),
                onClickShow = {}
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun ShowItemContentDarkPreview() {
    TVShowsTheme(darkTheme = true) {
        Surface {
            ShowItemContent(
                show = ViewShow.dummy(),
                onClickShow = {}
            )
        }
    }
}