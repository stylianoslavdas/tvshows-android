package com.test.tvshows.ui.showdetail

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.test.data.Result.Success
import com.test.data.Result.Error
import com.test.data.source.MyRepository
import com.test.tvshows.mapper.toViewModel
import com.test.tvshows.model.ViewShow
import com.test.tvshows.ui.graph.HomeDestinationsArgs
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * UiState for the Details screen.
 */
data class ShowDetailUiState(
    val show: ViewShow? = null,
    val loading: Boolean = false,
    val message: String? = null
)

/**
 * ViewModel for the Details screen.
 */
@HiltViewModel
class ShowDetailViewModel @Inject constructor(
    private val myRepository: MyRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val showId: String = savedStateHandle[HomeDestinationsArgs.SHOW_ID_ARG]!!

    private val _uiState = MutableStateFlow(ShowDetailUiState())
    val uiState: StateFlow<ShowDetailUiState> = _uiState.asStateFlow()

    init {
        getShow(showId.toLong())
    }

    fun snackbarMessageShown() {
        _uiState.update {
            it.copy(message = null)
        }
    }

    fun getShow(showId: Long) {
        _uiState.update {
            it.copy(loading = true)
        }
        viewModelScope.launch {
            when(val showResult = myRepository.getShowById(showId)) {
                is Success -> {
                    _uiState.update {
                        it.copy(show = showResult.data.toViewModel(), loading = false)
                    }
                }
                is Error -> {
                    _uiState.update {
                        it.copy(loading = false, message = showResult.exception.message)
                    }
                }
            }
        }
    }
}