package com.test.tvshows.ui.shows

import android.app.Activity
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.test.tvshows.R
import com.test.tvshows.model.ViewShow
import com.test.tvshows.ui.theme.TVShowsTheme
import com.test.tvshows.util.LoadingContent
import com.test.tvshows.util.TopAppBarContent

@Composable
fun ShowsScreen(
    onShowClick: (ViewShow) -> Unit,
    onFinish: (Activity) -> Unit,
    activity: Activity? = (LocalContext.current as? Activity),
    viewModel: ShowsViewModel = hiltViewModel(),
    scaffoldState: ScaffoldState = rememberScaffoldState()
) {
    Scaffold(
        scaffoldState = scaffoldState,
        modifier = Modifier.fillMaxSize()
    ) { paddingValues ->

        val uiState by viewModel.uiState.collectAsStateWithLifecycle()

        uiState.message?.let { message ->
            LaunchedEffect(scaffoldState, viewModel, message) {
                scaffoldState.snackbarHostState.showSnackbar(message)
                viewModel.snackbarMessageShown()
            }
        }

        Column(
            modifier = Modifier.fillMaxSize().padding(paddingValues)
        ) {
            TopAppBarContent(
                title = stringResource(R.string.shows_title),
                onClickIcon = {
                    activity?.let {
                        onFinish(it)
                    }
                }
            )
            ShowsContent(
                loading = uiState.loading,
                shows = uiState.shows,
                onRefresh = viewModel::refresh,
                onShowClick = onShowClick,
                modifier = Modifier.padding(paddingValues)
            )
        }
    }
}

@Composable
private fun ShowsContent(
    loading: Boolean,
    shows: List<ViewShow>,
    onRefresh: () -> Unit,
    onShowClick: (ViewShow) -> Unit,
    modifier: Modifier = Modifier
) {
    val state = rememberLazyListState()
    LoadingContent(
        loading = loading,
        empty = shows.isEmpty() && !loading,
        emptyContent = { ShowsEmptyContent(R.string.generic_noData, modifier) },
        onRefresh = onRefresh
    ) {
        val firstItemTranslationY by remember {
            derivedStateOf {
                when {
                    state.layoutInfo.visibleItemsInfo.isNotEmpty() && state.firstVisibleItemIndex == 0 -> state.firstVisibleItemScrollOffset * .6f
                    else -> 0f
                }
            }
        }
        val visibility by remember {
            derivedStateOf {
                when {
                    state.layoutInfo.visibleItemsInfo.isNotEmpty() && state.firstVisibleItemIndex == 0 -> {
                        val imageSize = state.layoutInfo.visibleItemsInfo[0].size
                        val scrollOffset = state.firstVisibleItemScrollOffset
                        scrollOffset / imageSize.toFloat()
                    }
                    else -> 1f
                }
            }
        }
        LazyColumn(
            state = state,
            verticalArrangement = Arrangement.spacedBy(16.dp),
            modifier = modifier.fillMaxSize()
        ) {
            item {
                ShowsInfoContent(
                    nShows = shows.size,
                    nTypeScripted = shows.filter { it.type == "Scripted" }.size,
                    nTypeAnimation = shows.filter { it.type == "Animation" }.size,
                    nTypeReality = shows.filter { it.type == "Reality" }.size,
                    nTypeTalkShow = shows.filter { it.type == "Talk Show" }.size,
                    modifier = Modifier
                        .fillParentMaxWidth()
                        .padding(top = 8.dp, start = 16.dp, end = 16.dp)
                        .graphicsLayer {
                            alpha = 1f - visibility*1.5f
                            scaleX = 1f - visibility/8
                            scaleY = 1f - visibility/8
                            translationY = firstItemTranslationY
                        }
                )
            }
            items(shows) { show ->
                ShowItemContent(
                    show = show,
                    onClickShow = onShowClick
                )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun ShowsContentPreview() {
    TVShowsTheme(darkTheme = false) {
        Surface {
            ShowsContent(
                loading = false,
                shows = listOf(ViewShow.dummy(), ViewShow.dummy()),
                onRefresh = {},
                onShowClick = {}
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun ShowsContentDarkPreview() {
    TVShowsTheme(darkTheme = true) {
        Surface {
            ShowsContent(
                loading = false,
                shows = listOf(ViewShow.dummy(), ViewShow.dummy(), ViewShow.dummy()),
                onRefresh = {},
                onShowClick = {}
            )
        }
    }
}