package com.test.tvshows.ui.showdetail

import androidx.annotation.StringRes
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.layoutId
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ExperimentalMotionApi
import androidx.constraintlayout.compose.MotionLayout
import androidx.constraintlayout.compose.MotionScene
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import coil.compose.AsyncImage
import com.test.tvshows.R
import com.test.tvshows.model.ViewShow
import com.test.tvshows.ui.shows.ShowsEmptyContent
import com.test.tvshows.ui.theme.TVShowsTheme
import com.test.tvshows.util.HtmlText
import com.test.tvshows.util.LoadingContent
import com.test.tvshows.util.TopAppBarContent

@Composable
fun ShowDetailScreen(
    onBack: () -> Unit,
    viewModel: ShowDetailViewModel = hiltViewModel(),
    scaffoldState: ScaffoldState = rememberScaffoldState()
) {
    Scaffold(
        scaffoldState = scaffoldState,
        modifier = Modifier.fillMaxSize()
    ) { paddingValues ->

        val uiState by viewModel.uiState.collectAsStateWithLifecycle()

        uiState.message?.let { message ->
            LaunchedEffect(scaffoldState, viewModel, message) {
                scaffoldState.snackbarHostState.showSnackbar(message)
                viewModel.snackbarMessageShown()
            }
        }

        Column(
            modifier = Modifier.fillMaxSize().padding(paddingValues)
        ) {
            TopAppBarContent(
                title = uiState.show?.name ?: "",
                icon = R.drawable.ic_close,
                onClickIcon = {
                    onBack()
                },
                modifier = Modifier.background(Color.Black)
            )
            ShowDetailContent(
                loading = uiState.loading,
                show = uiState.show,
                modifier = Modifier.padding(paddingValues)
            )
        }
    }
}

@Composable
private fun ShowDetailContent(
    loading: Boolean,
    show: ViewShow?,
    modifier: Modifier = Modifier
) {
    val scrollState = rememberScrollState()
    LoadingContent(
        loading = loading,
        empty = show == null && !loading,
        emptyContent = { ShowsEmptyContent(R.string.generic_noData, modifier) },
        onRefresh = {}
    ) {
        Column(
            modifier = modifier.fillMaxSize()
        ) {
            if (show != null) {
                HeaderContent(
                    show = show,
                    progress = (scrollState.value.toFloat() / scrollState.maxValue)
                )
                Column(
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier
                        .verticalScroll(scrollState)
                ) {
                    HtmlText(
                        html = show.summary,
                        style = MaterialTheme.typography.body2,
                        textColor = MaterialTheme.colors.onBackground,
                        modifier = Modifier.padding(top = 16.dp, start = 16.dp, end = 16.dp)
                    )
                    LazyRow(
                        contentPadding = PaddingValues(16.dp),
                        horizontalArrangement = Arrangement.spacedBy(8.dp)
                    ) {
                        items(show.genres) { genre ->
                            Box(
                                modifier = Modifier
                                    .clip(shape = RoundedCornerShape(16.dp))
                                    .background(MaterialTheme.colors.onBackground)
                            ) {
                                Text(
                                    text = genre,
                                    color = MaterialTheme.colors.background,
                                    modifier = Modifier.padding(top = 8.dp, bottom = 8.dp, start = 16.dp, end = 16.dp)
                                )
                            }
                        }
                    }
                    InfoItemContent(title = R.string.show_detail_type, value = show.type)
                    InfoItemContent(title = R.string.show_detail_language, value = show.language)
                    InfoItemContent(title = R.string.show_detail_status, value = show.status)
                    InfoItemContent(title = R.string.show_detail_runtime, value = show.runtime.toString())
                    InfoItemContent(title = R.string.show_detail_premiered, value = show.getPremieredFormatted())
                    Divider(modifier = Modifier.padding(16.dp))
                    Text(
                        text = stringResource(R.string.show_detail_officialSite),
                        style = MaterialTheme.typography.h6,
                    )
                    Text(
                        text = show.officialSite,
                        style = MaterialTheme.typography.body2,
                        modifier = Modifier.padding(bottom = 16.dp)
                    )
                }
            }
        }
    }
}


@OptIn(ExperimentalMotionApi::class)
@Composable
private fun HeaderContent(
    show: ViewShow,
    progress: Float
) {
    val context = LocalContext.current
    val motionScene = remember {
        context.resources
            .openRawResource(R.raw.motion_scene)
            .readBytes()
            .decodeToString()
    }
    MotionLayout(
        motionScene = MotionScene(content = motionScene),
        progress = progress,
        modifier = Modifier.fillMaxWidth()
    ) {
        val properties = motionProperties(id = "rating_star")
        AsyncImage(
            model = show.image.original,
            contentDescription = "contentDescription",
            modifier = Modifier
                .fillMaxWidth()
                .height(350.dp)
                .background(Color.Black)
                .layoutId("image")
        )
        Image(
            painter = painterResource(id = R.drawable.ic_star),
            contentDescription = null,
            colorFilter = ColorFilter.tint(properties.value.color("color")),
            modifier = Modifier
                .layoutId("rating_star")
        )
        Text(
            text = show.rating.toString(),
            color = Color.White,
            modifier = Modifier
                .layoutId("rating_value")
        )
    }
}

@Composable
private fun InfoItemContent(@StringRes title: Int, value: String) {
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp)
    ) {
        Text(
            text = stringResource(title),
            style = MaterialTheme.typography.subtitle1,
        )
        Text(
            text = value,
            style = MaterialTheme.typography.body2,
        )
    }
}

@Preview(showBackground = true)
@Composable
private fun ShowDetailContentPreview() {
    TVShowsTheme(darkTheme = false) {
        Surface {
            ShowDetailContent(
                loading = false,
                show = ViewShow.dummy()
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun ShowDetailContentDarkPreview() {
    TVShowsTheme(darkTheme = true) {
        Surface {
            ShowDetailContent(
                loading = false,
                show = ViewShow.dummy()
            )
        }
    }
}