package com.test.tvshows.ui.theme

import androidx.compose.ui.graphics.Color

val BasicRed = Color(0xFFE42229)
val BasicRedLight = Color(0xFFF96943)
val BasicGreen = Color(0xFF90EE90)