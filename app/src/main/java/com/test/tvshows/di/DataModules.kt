package com.test.tvshows.di

import android.content.Context
import androidx.room.Room
import com.test.data.source.DefaultMyRepository
import com.test.data.source.MyRepository
import com.test.data.source.local.DefaultMyLocalDataSource
import com.test.data.source.local.MyDatabase
import com.test.data.source.local.MyLocalDataSource
import com.test.data.source.remote.DefaultMyRemoteDataSource
import com.test.data.source.remote.MyRemoteDataSource
import com.test.data.network.NetworkAPI
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Qualifier
import javax.inject.Singleton

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class RemoteMyDataSource

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class LocalMyDataSource

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Singleton
    @Provides
    fun provideMyRepository(
        @RemoteMyDataSource myRemoteDataSource: MyRemoteDataSource,
        @LocalMyDataSource myLocalDataSource: MyLocalDataSource,
        @IoDispatcher ioDispatcher: CoroutineDispatcher
    ): MyRepository {
        return DefaultMyRepository(myRemoteDataSource, myLocalDataSource, ioDispatcher)
    }
}

@Module
@InstallIn(SingletonComponent::class)
object DataSourceModule {

    @Singleton
    @RemoteMyDataSource
    @Provides
    fun provideMyRemoteDataSource(
        networkAPI: NetworkAPI,
        @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
    ): MyRemoteDataSource = DefaultMyRemoteDataSource(networkAPI, defaultDispatcher)

    @Singleton
    @LocalMyDataSource
    @Provides
    fun provideMyLocalDataSource(
        database: MyDatabase,
        @IoDispatcher ioDispatcher: CoroutineDispatcher
    ): MyLocalDataSource {
        return DefaultMyLocalDataSource(database.showsDao(), ioDispatcher)
    }
}

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Singleton
    @Provides
    fun provideDataBase(@ApplicationContext context: Context): MyDatabase {
        return Room.databaseBuilder(
            context.applicationContext,
            MyDatabase::class.java,
            "MyDatabase.db"
        ).fallbackToDestructiveMigration().build()
    }
}