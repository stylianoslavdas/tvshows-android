package com.test.tvshows.model

import java.text.SimpleDateFormat
import java.util.*

data class ViewShow(
    val id: Long,
    val url: String,
    val name:  String,
    val type: String,
    val language: String,
    val genres: List<String>,
    val status: String,
    val runtime: Int,
    val rating: Double,
    val premiered: Date,
    val officialSite: String,
    val image: Image,
    val summary: String
) {
    data class Image(
        val medium: String,
        val original: String
    )

    fun getPremieredFormatted(): String {
        return SimpleDateFormat("dd MMM yyyy", Locale.getDefault()).format(premiered)
    }

    companion object {
        fun dummy(): ViewShow {
            return ViewShow(
                id = 1,
                url = "https://www.tvmaze.com/shows/1/under-the-dome",
                name = "Under the Dome",
                type = "Scripted",
                language = "English",
                genres = listOf("Drama", "Science-Fiction", "Thriller"),
                status = "Ended",
                runtime = 60,
                rating = 6.5,
                premiered = Date(),
                officialSite = "http://www.cbs.com/shows/under-the-dome/",
                image = Image(
                    medium = "https://static.tvmaze.com/uploads/images/medium_portrait/81/202627.jpg",
                    original = "https://static.tvmaze.com/uploads/images/original_untouched/81/202627.jpg"
                ),
                summary = "<p><b>Under the Dome</b> is the story of a small town that is suddenly and inexplicably sealed off from the rest of the world by an enormous transparent dome. The town's inhabitants must deal with surviving the post-apocalyptic conditions while searching for answers about the dome, where it came from and if and when it will go away.</p>"
            )
        }
    }
}