package com.test.tvshows.util

import android.text.TextUtils
import android.widget.TextView
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.text.HtmlCompat
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import com.test.tvshows.R
import com.test.tvshows.ui.theme.TVShowsTheme

@Composable
fun HtmlText(html: String, style: TextStyle = MaterialTheme.typography.body1, textColor: Color, maxLines: Int? = null, modifier: Modifier = Modifier) {
    AndroidView(
        modifier = modifier,
        factory = { context -> TextView(context).apply {
            textSize = style.fontSize.value
            maxLines?.let {
                setLines(it)
                ellipsize = TextUtils.TruncateAt.END
            }
            setTextColor(textColor.toArgb())
        } },
        update = { it.text = HtmlCompat.fromHtml(html, HtmlCompat.FROM_HTML_MODE_COMPACT) }
    )
}

@Composable
fun LoadingContent(
    loading: Boolean,
    empty: Boolean,
    emptyContent: @Composable () -> Unit,
    onRefresh: () -> Unit,
    modifier: Modifier = Modifier,
    content: @Composable () -> Unit
) {
    if (empty) {
        emptyContent()
    } else {
        SwipeRefresh(
            state = rememberSwipeRefreshState(loading),
            onRefresh = onRefresh,
            modifier = modifier,
            content = content,
        )
    }
}

@Composable
fun TopAppBarContent(
    modifier: Modifier = Modifier,
    title: String,
    icon: Int = R.drawable.ic_logout,
    onClickIcon: () -> Unit
) {
    Card(
        shape = RoundedCornerShape(bottomStart = 24.dp, bottomEnd = 24.dp),
        backgroundColor = MaterialTheme.colors.background,
        elevation = 8.dp,
        modifier = modifier.fillMaxWidth()
    ) {
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.fillMaxWidth().padding(8.dp)
        ) {
            Image(
                painter = painterResource(id = R.drawable.ic_local_movies),
                contentDescription = "Logo",
                colorFilter = ColorFilter.tint(MaterialTheme.colors.primary),
                modifier = Modifier.height(40.dp).width(40.dp)
            )
            Text(
                text = title,
                color = MaterialTheme.colors.onBackground
            )
            Image(
                painter = painterResource(id = icon),
                contentDescription = "Exit",
                colorFilter = ColorFilter.tint(MaterialTheme.colors.onBackground),
                modifier = Modifier.clickable {
                    onClickIcon()
                }
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun HomeScreenPreview() {
    TVShowsTheme(darkTheme = false) {
        TopAppBarContent(
            title = "Title",
            onClickIcon = {}
        )
    }
}

@Preview(showBackground = true)
@Composable
private fun HomeScreenDarkPreview() {
    TVShowsTheme(darkTheme = true) {
        TopAppBarContent(
            title = "Title",
            onClickIcon = {}
        )
    }
}

