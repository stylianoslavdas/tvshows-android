package com.test.tvshows

import androidx.annotation.VisibleForTesting
import com.test.data.Result
import com.test.data.Result.Success
import com.test.data.Result.Error
import com.test.data.model.local.Show
import com.test.data.source.MyRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.update

/**
 * Implementation of a remote data source with static access to the data for easy testing.
 */
class FakeRepository : MyRepository {

    private var shouldReturnError = false

    private val _savedShows = MutableStateFlow(LinkedHashMap<Long, Show>())
    val savedShows: StateFlow<LinkedHashMap<Long, Show>> = _savedShows.asStateFlow()

    fun setReturnError(value: Boolean) {
        shouldReturnError = value
    }

    override fun getShowsStream(): Flow<List<Show>> = savedShows.map {
        it.values.toList()
    }

    override suspend fun checkShowsFetch(): Result<Unit> {
        // Shows already fetched
        if (shouldReturnError) {
            return Error(Exception("Test exception"))
        }
        return Success(Unit)
    }

    override suspend fun fetchShows(): Result<Unit> {
        // Shows already fetched
        if (shouldReturnError) {
            return Error(Exception("Test exception"))
        }
        return Success(Unit)
    }

    override suspend fun getShowById(showId: Long): Result<Show> {
        if (shouldReturnError) {
            return Error(Exception("Test exception"))
        }
        savedShows.value[showId]?.let {
            return Success(it)
        }
        return Error(Exception("Could not find task"))
    }

    @VisibleForTesting
    fun addShows(vararg shows: Show) {
        _savedShows.update { oldShows ->
            val newShows = LinkedHashMap<Long, Show>(oldShows)
            for (show in shows) {
                newShows[show.id] = show
            }
            newShows
        }
    }
}
