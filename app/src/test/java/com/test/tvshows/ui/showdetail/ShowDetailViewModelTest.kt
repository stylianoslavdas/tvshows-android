package com.test.tvshows.ui.showdetail

import androidx.lifecycle.SavedStateHandle
import com.test.data.model.local.Show
import com.test.tvshows.FakeRepository
import com.test.tvshows.MainCoroutineRule
import com.test.tvshows.ui.graph.HomeDestinationsArgs
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Rule
import org.junit.Test

/**
 * Unit tests for the implementation of [ShowDetailViewModel]
 */
@ExperimentalCoroutinesApi
class ShowDetailViewModelTest {

    // Set the main coroutines dispatcher for unit testing.
    @ExperimentalCoroutinesApi
    @get:Rule
    val mainCoroutineRule = MainCoroutineRule()

    // Subject under test
    private lateinit var showDetailViewModel: ShowDetailViewModel

    // Use a fake repository to be injected into the viewmodel
    private lateinit var myRepository: FakeRepository
    private val show = Show.dummy(1)

    @Before
    fun setupViewModel() {
        myRepository = FakeRepository()
        myRepository.addShows(show)

        showDetailViewModel = ShowDetailViewModel(
            myRepository,
            SavedStateHandle(mapOf(HomeDestinationsArgs.SHOW_ID_ARG to "1"))
        )
    }

    @Test
    fun getShowFromRepositoryAndLoadIntoView() = runTest {
        val uiState = showDetailViewModel.uiState.first()
        // Then verify that the view was notified
        assertThat(uiState.show?.url).isEqualTo(show.url)
        assertThat(uiState.show?.name).isEqualTo(show.name)
        assertThat(uiState.show?.type).isEqualTo(show.type)
        assertThat(uiState.show?.language).isEqualTo(show.language)
        assertThat(uiState.show?.genres).isEqualTo(show.genres)
        assertThat(uiState.show?.runtime).isEqualTo(show.runtime)
        assertThat(uiState.show?.rating).isEqualTo(show.rating)
        assertThat(uiState.show?.premiered).isEqualTo(show.premiered)
        assertThat(uiState.show?.officialSite).isEqualTo(show.officialSite)
        assertThat(uiState.show?.image?.original).isEqualTo(show.image.original)
        assertThat(uiState.show?.summary).isEqualTo(show.summary)
    }

    @Test
    fun showDetailViewModel_repositoryError() = runTest {
        // Given a repository that returns errors
        myRepository.setReturnError(true)

        showDetailViewModel.getShow(1)

        // Then progress indicator is hidden
        assertThat(showDetailViewModel.uiState.first().loading).isFalse()
        assertThat(showDetailViewModel.uiState.value.message).isEqualTo("Test exception")
    }

    @Test
    fun loadShow_loading() = runTest {
        // Set Main dispatcher to not run coroutines eagerly, for just this one test
        Dispatchers.setMain(StandardTestDispatcher())

        var loading: Boolean? = true
        val job = launch {
            showDetailViewModel.uiState.collect {
                loading = it.loading
            }
        }

        // Then progress indicator is shown
        assertThat(loading).isTrue()

        // Execute pending coroutines actions
        advanceUntilIdle()

        // Then progress indicator is hidden
        assertThat(loading).isFalse()
        job.cancel()
    }
}