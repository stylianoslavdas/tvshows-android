package com.test.tvshows.ui.shows

import com.test.data.model.local.Show
import com.test.tvshows.FakeRepository
import com.test.tvshows.MainCoroutineRule
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.flow.first
import org.junit.Before
import org.junit.Rule
import org.junit.Test

/**
 * Unit tests for the implementation of [ShowsViewModel]
 */
@ExperimentalCoroutinesApi
class ShowsViewModelTest {

    // Subject under test
    private lateinit var showsViewModel: ShowsViewModel

    // Use a fake repository to be injected into the viewmodel
    private lateinit var myRepository: FakeRepository

    // Set the main coroutines dispatcher for unit testing.
    @ExperimentalCoroutinesApi
    @get:Rule
    val mainCoroutineRule = MainCoroutineRule()

    @Before
    fun setupViewModel() {
        // We initialise the tasks to 3, with one active and two completed
        myRepository = FakeRepository()
        val show1 = Show.dummy(1)
        val show2 = Show.dummy(2)
        val show3 = Show.dummy(3)
        myRepository.addShows(show1, show2, show3)

        showsViewModel = ShowsViewModel(myRepository)
    }

    @Test
    fun fetchShowsFromRepositoryAndDataLoaded() = runTest {
        // Set Main dispatcher to not run coroutines eagerly, for just this one test
        Dispatchers.setMain(StandardTestDispatcher())

        // Then progress indicator is shown
        assertThat(showsViewModel.uiState.first().loading).isTrue()

        // Execute pending coroutines actions
        advanceUntilIdle()

        // Then progress indicator is hidden
        assertThat(showsViewModel.uiState.first().loading).isFalse()

        // And data correctly loaded
        assertThat(showsViewModel.uiState.first().shows).hasSize(3)
    }

    @Test
    fun fetchShowsFromRepository_loadingTogglesAndDataLoaded() = runTest {
        // Set Main dispatcher to not run coroutines eagerly, for just this one test
        Dispatchers.setMain(StandardTestDispatcher())

        // Trigger loading of shows
        showsViewModel.refresh()

        // Then progress indicator is shown
        assertThat(showsViewModel.uiState.first().loading).isTrue()

        // Execute pending coroutines actions
        advanceUntilIdle()

        // Then progress indicator is hidden
        assertThat(showsViewModel.uiState.first().loading).isFalse()

        // And data correctly loaded
        assertThat(showsViewModel.uiState.first().shows).hasSize(3)
    }

    @Test
    fun loadShows_error() = runTest {
        // Make the repository return errors
        myRepository.setReturnError(true)

        // Load tasks
        showsViewModel.refresh()

        // Then progress indicator is hidden
        assertThat(showsViewModel.uiState.first().loading).isFalse()

        assertThat(showsViewModel.uiState.first().message).isEqualTo("Test exception")
    }
}